package structural.decorator;

/**
 * 
 * @author BMishra
 *
 */
public interface Pizza {
    public double getPrice();
}
