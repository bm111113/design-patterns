package decorator;

public class JalapenoTopping extends ToppingsDecorator {

    public JalapenoTopping(Pizza pizza) {
	super(pizza);
	this.price = 2.3;
    }

}
