package decorator;

public class MushroomToppings extends ToppingsDecorator {

    public MushroomToppings(Pizza pizza) {
	super(pizza);
	this.price = 2.6;
    }

}
