package decorator;

/**
 * 
 * @author BMishra
 *
 */
public class Margherita implements Pizza {

    @Override
    public double getPrice() {
	return 9.9;
    }

}
