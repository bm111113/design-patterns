package decorator;

/**
 * 
 * @author BMishra
 *
 */
public class ExtraCheeseToppings extends ToppingsDecorator {

    public ExtraCheeseToppings(Pizza pizza) {
	super(pizza);
	this.price = 1.2;
    }

}
