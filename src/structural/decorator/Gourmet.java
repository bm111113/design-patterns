package decorator;

/**
 * 
 * @author BMishra
 *
 */
public class Gourmet implements Pizza {

    @Override
    public double getPrice() {
	return 10.10;
    }

}
