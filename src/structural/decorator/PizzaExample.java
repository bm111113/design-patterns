package decorator;

public class PizzaExample {
    public static void main(String args[]) {
	Pizza margerita = new Margherita();
	System.out.println("Price of margherita: " + margerita.getPrice());

	Pizza extraCheeseMargherita = new ExtraCheeseToppings(margerita);
	System.out.println("Price of margherita with extra chees: "
		+ extraCheeseMargherita.getPrice());

	Pizza mushroomToppings = new MushroomToppings(extraCheeseMargherita);
	System.out.println("Price of extra cheese margherita with mushrooms: "
		+ mushroomToppings.getPrice());

	Pizza jalapenoToppings = new JalapenoTopping(extraCheeseMargherita);
	System.out.println("Price of extra cheese marghertia with jalapenos: "
		+ jalapenoToppings.getPrice());

	Pizza gourmet = new Gourmet();
	System.out.println("Price of gourmet: " + margerita.getPrice());

	Pizza extraCheeseGourmet = new ExtraCheeseToppings(gourmet);
	System.out.println("Price of gourmet with extra chees: "
		+ extraCheeseGourmet.getPrice());

	Pizza mushroomGourmet = new MushroomToppings(extraCheeseGourmet);
	System.out.println("Price of extra cheese gourmet with mushrooms: "
		+ mushroomGourmet.getPrice());

	Pizza jalapenoGourmet = new JalapenoTopping(extraCheeseGourmet);
	System.out.println("Price of extra cheese gourmet with jalapenos: "
		+ jalapenoGourmet.getPrice());
    }
}
