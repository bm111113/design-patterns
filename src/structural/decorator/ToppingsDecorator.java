package decorator;

public class ToppingsDecorator implements Pizza {

    protected double price;
    private Pizza pizza;

    public ToppingsDecorator(Pizza pizza) {
	this.pizza = pizza;
    }

    @Override
    public double getPrice() {
	return this.pizza.getPrice() + this.price;
    }
}
