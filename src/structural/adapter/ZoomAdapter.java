package adapter;

import java.net.MalformedURLException;

import adapter.adapter3rdparty.ZoomAPI;

/**
 * 
 * @author BMishra
 *
 */
public class ZoomAdapter implements CalendlyAPIAdapter {
    private ZoomAPI zoom;
    private String spec;

    public ZoomAdapter() {
	zoom = new ZoomAPI();
    }

    public ZoomAdapter(String spec) {
	zoom = new ZoomAPI();
	this.spec = spec;
    }

    @Override
    public boolean isValidUser() {
	return zoom.isValidUser() == 1 ? true : false;
    }

    @Override
    public String getMeetingLink() throws MalformedURLException {
	return zoom.generateMeetingLink(spec).toString();
    }
}
