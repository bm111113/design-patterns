package adapter.adapter3rdparty;

import java.net.MalformedURLException;
import java.net.URL;

public class ZoomAPI {
    public URL generateMeetingLink(String spec) throws MalformedURLException {
	return new URL(spec);
    }

    public int isValidUser() {
	return 1;
    }
}
