package adapter;

import java.net.MalformedURLException;

import exception.InvalidUserException;

public class Calendly {
    private CalendlyAPIAdapter calendlyAdapter;

    public Calendly(AdapterType adapterType) {
	calendlyAdapter = AdapterFactory.getCalendlyAdapter(adapterType);
    }

    public String getMeetingLink() throws MalformedURLException {
	if (calendlyAdapter.isValidUser()) {
	    return calendlyAdapter.getMeetingLink();
	} else
	    throw new InvalidUserException("Invalid User");
    }
}
