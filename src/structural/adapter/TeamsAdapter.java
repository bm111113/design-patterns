package adapter;

import java.net.MalformedURLException;

import adapter.adapter3rdparty.TeamsAPI;

public class TeamsAdapter implements CalendlyAPIAdapter {

    private TeamsAPI teams;

    public TeamsAdapter() {
	teams = new TeamsAPI();
    }

    @Override
    public boolean isValidUser() {
	return teams.isValidUser();
    }

    @Override
    public String getMeetingLink() throws MalformedURLException {
	return teams.meetingLink().toString();
    }

}
