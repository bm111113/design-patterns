package adapter;

/**
 * 
 * @author BMishra
 *
 */
public enum AdapterType {
    Google(1), Teams(2), Zoom(3);

    int value;

    AdapterType(int value) {
	this.value = value;
    }

    public int getValue() {
	return value;
    }
}
