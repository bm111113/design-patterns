package adapter;

public class AdapterFactory {
    private AdapterFactory() {
    }

    public static CalendlyAPIAdapter getCalendlyAdapter(
	    AdapterType adapterType) {
	switch (adapterType) {
	case Google:
	    return new GoogleMeetAdapter();
	case Teams:
	    return new TeamsAdapter();
	case Zoom:
	    return new ZoomAdapter();
	}
	return null;
    }
}
