package adapter;

import java.net.MalformedURLException;

import adapter.adapter3rdparty.GoogleMeetAPI;

public class GoogleMeetAdapter implements CalendlyAPIAdapter {
    private GoogleMeetAPI googleMeet;

    public GoogleMeetAdapter() {
	googleMeet = new GoogleMeetAPI();
    }

    @Override
    public boolean isValidUser() {
	return googleMeet.authenticateUser();
    }

    @Override
    public String getMeetingLink() throws MalformedURLException {
	return googleMeet.getMeetingLink();
    }
}
