package adapter;

import java.net.MalformedURLException;

/**
 * 
 * @author BMishra
 *
 */
public interface CalendlyAPIAdapter {
    public boolean isValidUser();

    public String getMeetingLink() throws MalformedURLException;
}
