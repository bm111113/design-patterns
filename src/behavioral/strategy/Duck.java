package behavioral.strategy;

public class Duck {
    IFlyBehavior flyBehavior;
    IQuackBehavior quackBehavior;

    public Duck(IFlyBehavior flyBehavior, IQuackBehavior quackBehavior) {
	this.flyBehavior = flyBehavior;
	this.quackBehavior = quackBehavior;
    }

    public void fly() {
	this.flyBehavior.fly();
    }

    public void quack() {
	this.quackBehavior.quack();
    }
}
