package behavioral.strategy;

public interface IFlyBehavior {
    public void fly();
}
