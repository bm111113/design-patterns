package creational.prototype;

import java.util.HashMap;
import java.util.Map;

public class TrainRegistry {
    private Map<String, LocalTrain> registry;
    private static TrainRegistry trains;

    private TrainRegistry() {
	registry = new HashMap<>();
    }

    public static TrainRegistry getInstance() {
	if (trains == null)
	    trains = new TrainRegistry();
	return trains;
    }

    public LocalTrain getTrain(String name) {
	return registry.get(name);
    }

    public void addTrain(String name, LocalTrain train) {
	registry.put(name, train);
    }
}
