package creational.prototype;

import java.util.HashMap;
import java.util.Map;

public class Client {
    public static void main(String[] args) {
	TrainRegistry registry = TrainRegistry.getInstance();
	LocalTrain Train10 = new LocalTrain();
	Train10.fare = 20.0;
	Train10.engineType = 1;
	Train10.trainType = 1;
	Train10.noOfSeats = 240;

	registry.addTrain("Ten", Train10);

	LocalTrain Train1015 = registry.getTrain("Ten").clone();
	Map<String, String> stations = new HashMap<String, String>();
	stations.put("Bandra", "10:00");
	stations.put("Andheri", "11:19");
	Train1015.setStations(stations);

	System.out.println(Train1015);
    }
}
