package creational.prototype;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class LocalTrain implements Prototype<LocalTrain> {
    double fare;
    int trainType;
    int engineType;
    int noOfSeats;
    Map<String, String> stations;

    public LocalTrain() {
	stations = new HashMap<String, String>();
	System.out.println("Default constructor called");
    }

    public LocalTrain(LocalTrain train) {
	this.fare = train.fare;
	this.engineType = train.engineType;
	this.trainType = train.trainType;
	this.noOfSeats = train.noOfSeats;

    }

    public LocalTrain clone() {
	return new LocalTrain(this);
    }

    public Map<String, String> getStations() {
	return stations;
    }

    public void setStations(Map<String, String> stations) {
	this.stations = stations;
    }

    @Override
    public String toString() {
	StringBuilder result = new StringBuilder("LocalTrain [fare=" + fare
		+ ", trainType=" + trainType + ", engineType=" + engineType
		+ ", noOfSeats=" + noOfSeats + "]");
	for (Entry<String, String> station : stations.entrySet())
	    result.append("\nStation: " + station.getKey() + ", Timing: "
		    + station.getValue());
	return result.toString();
    }
}