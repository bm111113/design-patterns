package creational.builder;

public class Client {
    public static void main(String[] args) {

	AircraftBuilder f16Builder = new F16Builder();
	Director director = new Director(f16Builder);
	director.construct(false);

	System.out.println(f16Builder.getResult());
    }
}
