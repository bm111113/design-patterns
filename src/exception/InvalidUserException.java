package exception;

/**
 * 
 * @author BMishra
 *
 */
public class InvalidUserException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = -7903769315719541423L;

    public InvalidUserException() {
	super();
    }

    public InvalidUserException(String message) {
	super(message);
    }

}
